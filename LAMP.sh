# ! /bin/bash
# Script encargado de instalar el entorno de trabajo LAMP en fedora 30
# Autor : José Luis Luna Rubio
# Sitio web: https://joseluislunarubio.net

#Informacion obtenida de https://computingforgeeks.com/how-to-install-lamp-stack-on-fedora/

function mostrarMenu()
{
	echo "----------------------------------"
	echo "----------------------------------"
	echo "    Instalacion LAMP fedora 30    "
	echo "----------------------------------"
	echo "L => Linux************(Sistema operativo)"
	echo "A => Apache***********(Servidor HTTP)"
	echo "M => MySQL / MariaDB**(Base de datos)"
	echo "P => PHP**************(Lenguaje de programacion)"
	echo "----------------------------------"
	echo "Saludos ".$USER ". Selecciona una opcion."
	read -s -p "Contraseña de Root : " pass
	echo "*******Menu*******"
	echo "1) Instalar paquetes basicos (vim, curl, wget, telnet)"
	echo "2) Instalar Apache Server"
	echo "3) Instalar PHP y Extensiones"
	echo "4) Instalar MySQL / MariaDB"
	echo "e) Salir del programa"
	read -p "Selecciona una opcion : " opcion

	case $opcion in 
		1)
			echo $pass
			#read -p "\n Ingrese contraseña de super usuario ROOT... " pass
			#echo "sd"
			#echo "sudo dnf -y update"
			#echo "sudo dnf -y install vim bash-completion curl wget telnet"
			#echo -e "\n Ingrese contraseña de super usuario ROOT"
			;;
		e)
			echo  "\n Saliendo del sistema"
			sleep 1
			exit 0
			;;
	esac
}

#Inicializamos la aplicacion

while :
do 
	clear
	mostrarMenu
done