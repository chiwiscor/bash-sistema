# ! /bin/bash
# Script encargado de mostrar informacion respecto al sistema del usuario
# Autor : Jose Luis Luna Rubio 
# Sitio web : https://joseluislunaurubio.net


function mostrarMenu() 
{
    echo "-------------------------------"
    echo "    Informacion del sistema    "     
    echo "-------------------------------"
    echo "1) Arquitectura de la maquina"
    echo "2) Version del Kernel"
    echo "e) Salir del sistema"
    read -p "Selecciona una opcion : "  opcion

    case $opcion in
        1)
            echo -e "\n Mostrando la informacion del sistema ..."    
            sleep 1
            echo  "Arch : $(arch)"            
            echo  "uname -m : $(uname -m)"
            read  -p "Presiona cualquier tecla para continuar" 
            ;;
        2)
            echo "Mosntrando el kernel..."
            sleep 1
            echo $(uname -r)
            read -p "Presiona cualquier tecla para continuar"
            ;;
        e)          
            echo  "\n Saliendo del sistema"
            sleep 1
            exit 0
            ;;
    esac
        
}



#Inicializamos la aplicacion
while :
do
    clear
    mostrarMenu
done
